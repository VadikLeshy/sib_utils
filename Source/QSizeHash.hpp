#ifndef QSIZEHASH_HPP
#define QSIZEHASH_HPP

#include <functional>
#include <utility>
#include <QSize>

namespace sib_utils {
  struct QSizeHash
  {
    std::size_t operator()(QSize const &val) const noexcept
    {
      static_assert(sizeof(QSize) == sizeof(uint64_t));
      return std::hash<uint64_t>{}(*reinterpret_cast<const uint64_t*>(&val));
    }
  };
}

#endif // QSIZEHASH_HPP
