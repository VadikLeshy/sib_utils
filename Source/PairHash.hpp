#ifndef PAIRHASH_HPP
#define PAIRHASH_HPP

#include <functional>
#include <utility>

namespace sib_utils {
  template<typename T1, typename T2>
  struct PairHash
  {
    std::size_t operator()(std::pair<T1, T2> const &val) const noexcept
    {
      std::size_t h1 = std::hash<T1>{}(val.first);
      std::size_t h2 = std::hash<T2>{}(val.second);
      return h1 ^ (h2 << 1);
    }
  };

  template<typename T1, typename T2>
  struct CustomPairHash
  {
    template<typename U1, typename U2>
    std::size_t operator()(std::pair<U1, U2> const &val) const noexcept
    {
      std::size_t h1 = T1{}(val.first);
      std::size_t h2 = T2{}(val.second);
      return h1 ^ (h2 << 1);
    }
  };
}

#endif // PAIRHASH_HPP
