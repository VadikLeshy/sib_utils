#ifndef QDATASTREAMEXPANSION_HPP
#define QDATASTREAMEXPANSION_HPP

#include "sib_utils_global.hpp"
#include <QDataStream>
#include <optional>
#include <cstddef>
#include <variant>
#include <vector>
#include <array>
#include <list>

SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, std::byte a);
SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::byte &a);

template<typename T>
QDataStream &operator<<(QDataStream &st, const std::optional<T> &a)
{
  if(a)
    return st << quint8(1) << *a;
  else
    return st << quint8(0);
}

template<typename T>
QDataStream &operator>>(QDataStream &st, std::optional<T> &a)
{
  quint8 ctrlByte = 3;
  st >> ctrlByte;
  switch (ctrlByte)
    {
    case 0:
      a = std::nullopt;
      break;

    case 1:
      {
        T val;
        st >> val;
        a = val;
      }
      break;

    default:
      st.setStatus(QDataStream::Status::ReadCorruptData);
      break;
    }
  return st;
}

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<double> &a);

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<double> &a);

template<typename T>
QDataStream &operator<<(QDataStream &st, const std::vector<T> &a)
{
  const quint64 sz = a.size();
  st << sz;
  for(const auto &i: a)
    st << i;
  return st;
}

template<typename T>
QDataStream &operator>>(QDataStream &st, std::vector<T> &a)
{
  quint64 sz;
  st >> sz;
  a.resize(sz);
  for(auto &i: a)
    st >> i;
  return st;
}

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<double> &a);

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<double> &a);

template<typename... ArgsT>
QDataStream& operator<<(QDataStream &st, const std::variant<ArgsT...> &val)
{
  st << static_cast<quint64>(val.index());
  std::visit([&st](auto &&arg) {st<<arg;}, val);
  return st;
}

template<quint64 I, typename... ArgsT>
void readVariantAlternative(QDataStream &st, quint64 i, std::variant<ArgsT...> &val)
{
  if constexpr(I < std::variant_size_v<std::variant<ArgsT...>>)
  {
    if(i == I)
      {
        std::variant_alternative_t<I, std::variant<ArgsT...>> v;
        st >> v;
        val.template emplace<I>(std::move(v));
      } else
      readVariantAlternative<I+1>(st, i, val);
  } else
  st.setStatus(QDataStream::Status::ReadCorruptData);
}

template<typename... ArgsT>
QDataStream& operator>>(QDataStream &st, std::variant<ArgsT...> &val)
{
  quint64 type = std::variant_size_v<std::variant<ArgsT...>> + 1;
  st >> type;
  readVariantAlternative<0>(st, type, val);
  return st;
}

template<typename T>
QDataStream &operator<<(QDataStream &st, const std::list<T> &a)
{
  const quint64 sz = a.size();
  st << sz;
  for(const auto &i: a)
    st << i;
  return st;
}

template<typename T>
QDataStream &operator>>(QDataStream &st, std::list<T> &a)
{
  quint64 sz;
  st >> sz;
  a.clear();
  for(quint64 i = 0; i < sz; ++i)
    {
      a.emplace_back();
      st >> a.back();
    }

  return st;
}

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<double> &a);

extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint8> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint16> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint32> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint64> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<float> &a);
extern template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<double> &a);

template<typename T, size_t N>
QDataStream &operator<<(QDataStream &st, const std::array<T, N> &a)
{
  for(const T &i: a)
    st << i;
  return st;
}

template<typename T, size_t N>
QDataStream &operator>>(QDataStream &st, std::array<T, N> &a)
{
  for(T &i: a)
    st >> i;
  return st;
}

#endif // QDATASTREAMEXPANSION_HPP
