#ifndef DIRTY_CAST_HPP
#define DIRTY_CAST_HPP

#include <type_traits>

namespace sib_utils {
  template<typename T, typename U>
  auto dirty_cast(U &&val)
  {
    if constexpr(std::is_pointer_v<decltype(val)> && std::is_pointer_v<T>)
    {
      if constexpr(std::is_const_v<std::remove_pointer_t<decltype(&val)>>)
      {
        return reinterpret_cast<const T&>(val);
      } else
      {
        return reinterpret_cast<T&>(val);
      }
    } else
    {
      static_assert(sizeof(T) == sizeof(U));
      if constexpr(std::is_const_v<std::remove_pointer_t<decltype(&val)>>)
      {
        return *reinterpret_cast<const T*>(&val);
      } else
      {
        return *reinterpret_cast<T*>(&val);
      }
    }
  }

  template<typename T>
  struct DirtyCaster {
    static inline auto f = [](auto &&val) {
        return dirty_cast<T>(val);
      };
  };
}

#endif // DIRTY_CAST_HPP
