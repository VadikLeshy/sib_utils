#include "Memento.hpp"
#include <functional>
#include <QtConcurrent>
using namespace sib_utils;
using namespace memento;

Memento::Memento(const QString &type):
  lock(QReadWriteLock::Recursive),
  type(type)
{
}

Memento::~Memento()
{
}

Memento::DataReader Memento::reader(const QHash<Memento*, TreeNode*> &createdObjects)
{
  return DataReader(*this, createdObjects);
}

Memento::DataMutator Memento::mutator(const QHash<const TreeNode*, Memento*> &createdMemento, bool preserveExternalLinks)
{
  return DataMutator(*this, createdMemento, preserveExternalLinks);
}

Memento::DataReader Memento::reader()
{
  return reader({});
}

Memento::DataMutator Memento::mutator(bool preserveExternalLinks)
{
  return mutator({}, preserveExternalLinks);
}



Memento::DataReader::DataReader(Memento &target, const QHash<Memento*, TreeNode*> &createdObjects):
  l(&target.lock),
  target(target),
  createdObjects(createdObjects)
{
}

Memento::DataReader &Memento::DataReader::get(const QString &key, QByteArray &out, bool &readed)
{
  auto el = target.binaryData.find(key);
  if(el != target.binaryData.end())
    {
      out = *el;
      readed = true;
    } else
    readed = false;

  return *this;
}

Memento::DataReader &Memento::DataReader::get(const QString &key, QString &out, bool &readed)
{
  auto el = target.textData.find(key);
  if(el != target.textData.end())
    {
      out = *el;
      readed = true;
    } else
    readed = false;

  return *this;
}

Memento::DataReader &Memento::DataReader::get(const QString &key, TreeNode *&out, bool &readed)
{
  Memento *mem;
  getRawLink(key, mem, readed);
  if(readed)
    if(auto el = createdObjects.find(mem); el != createdObjects.end())
      {
        out = createdObjects.value(mem, nullptr);
        return *this;
      }

  if(auto el = target.externalLinksData.find(key); el != target.externalLinksData.end())
    out = *el;
  return *this;
}

Memento::DataReader &Memento::DataReader::getRawLink(const QString &key, Memento *&out, bool &readed)
{
  auto el = target.linksData.find(key);
  if(el != target.linksData.end())
    {
      out = *el;
      readed = true;
    } else
    readed = false;

  return *this;
}

Memento::DataReader &Memento::DataReader::read(const QString &key, std::function<void (const QByteArray &)> &&func)
{
  auto el = target.binaryData.find(key);
  if(el != target.binaryData.end())
    func(*el);

  return *this;
}

Memento::DataReader &Memento::DataReader::readText(const QString &key, std::function<void (const QString &)> &&func)
{
  auto el = target.textData.find(key);
  if(el != target.textData.end())
    func(*el);

  return *this;
}

Memento::DataReader &Memento::DataReader::read(const QString &key, std::function<void(TreeNode*)> &&func)
{
  if(auto el = target.linksData.find(key); el != target.linksData.end())
    if(auto el2 = createdObjects.find(*el); el2 != createdObjects.end())
      {
        func(*el2);
        return *this;
      }

  if(auto el = target.externalLinksData.find(key); el != target.externalLinksData.end())
    func(*el);
  else
    func(nullptr);

  return *this;
}

namespace {
  template <typename Iterator, typename MapFunctor>
  class KeyValueMapKernel : public QtConcurrent::IterateKernel<Iterator, void>
  {
    MapFunctor map;
  public:
    using ReturnType = void;

    KeyValueMapKernel(Iterator begin, Iterator end, MapFunctor _map):
      QtConcurrent::IterateKernel<Iterator, void>(QThreadPool::globalInstance(), begin, end),
      map(_map)
    { }

    bool runIteration(Iterator it, int, void *) override
    {
      map(it.key(), it.value());
      return false;
    }
  };

  template <typename Iterator, typename MapFunctor>
  KeyValueMapKernel<Iterator, MapFunctor>*
  makeKeyValueMapIterator(Iterator begin, Iterator end, MapFunctor _map)
  {
    return new KeyValueMapKernel<Iterator, MapFunctor>(begin, end, _map);
  }
}

Memento::DataReader &Memento::DataReader::readAll(std::function<void (const QString &, const QByteArray &)> &&func)
{
  auto &seq = target.binaryData;
  QFuture<void> task = QtConcurrent::startThreadEngine(makeKeyValueMapIterator(seq.constBegin(), seq.constEnd(), func));
  task.waitForFinished();
  return *this;
}

Memento::DataReader &Memento::DataReader::readAllText(std::function<void (const QString &, const QString &)> &&func)
{
  auto &seq = target.textData;
  QFuture<void> task = QtConcurrent::startThreadEngine(makeKeyValueMapIterator(seq.constBegin(), seq.constEnd(), func));
  task.waitForFinished();
  return *this;
}

Memento::DataReader &Memento::DataReader::readAllRawLinks(std::function<void (const QString &, Memento *)> &&func)
{
  auto &seq = target.linksData;
  QFuture<void> task = QtConcurrent::startThreadEngine(makeKeyValueMapIterator(seq.constBegin(), seq.constEnd(), func));
  task.waitForFinished();

  return *this;
}

Memento::DataReader &Memento::DataReader::readAll(std::function<void (const QString&, TreeNode*)> &&func)
{
  const auto &seq = target.linksData;
  QFuture<void> task = QtConcurrent::startThreadEngine(
        makeKeyValueMapIterator(seq.constBegin(),
                                seq.constEnd(),
                                [&func, this] (const QString &key, Memento *value) { func(key, createdObjects.value(value, nullptr)); }
  ));

  const auto &seq2 = target.externalLinksData;
  QFuture<void> task2 = QtConcurrent::startThreadEngine(
        makeKeyValueMapIterator(seq2.constBegin(), seq2.constEnd(),
                                [&func, &seq] (const QString &key, TreeNode *value) { if(!seq.contains(key)) func(key, value); }
  ));

  task.waitForFinished();
  task2.waitForFinished();

  return *this;
}

Memento::DataReader &Memento::DataReader::forallChildren(std::function<void (Memento &)> &&func)
{
  QtConcurrent::map(target.children, [&func](Memento *m) { func(*m); }).waitForFinished();
  return *this;
}

Memento::DataReader &Memento::DataReader::forallChildrenR(std::function<void (Memento &)> &&func)
{
  QtConcurrent::map(target.children.rbegin(), target.children.rend(), [&func](Memento *m) { func(*m); }).waitForFinished();
  return *this;
}

Memento::DataReader &Memento::DataReader::getChildrenCount(size_t &count)
{
  count = static_cast<size_t>(target.children.size());
  return *this;
}

Memento::DataReader &Memento::DataReader::getChild(size_t i, Memento *&result)
{
  if(i >= static_cast<size_t>(target.children.size()))
    throw std::out_of_range("Memento::DataReader::getAt: index out of range");

  result = target.children.at(i);
  return *this;
}

Memento::DataReader &Memento::DataReader::getType(QString &out)
{
  out = target.type;
  return *this;
}

Memento::DataReader &Memento::DataReader::readType(std::function<void (const QString &)> &&func)
{
  func(target.type);
  return *this;
}

const QHash<Memento*, TreeNode*> &Memento::DataReader::getCreatedObjects() const
{
  return createdObjects;
}



Memento::DataMutator::DataMutator(Memento &target, const QHash<const TreeNode*, Memento*> &createdMemento, bool preserveExternalLinks):
  l(&target.lock),
  target(target),
  createdMemento(createdMemento),
  preserveExternalLinks(preserveExternalLinks)
{
}

Memento::DataMutator &Memento::DataMutator::setData(const QString &key, const QByteArray &data)
{
  target.binaryData[key] = data;
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeData(const QString &key)
{
  target.binaryData.remove(key);
  target.textData.remove(key);
  target.linksData.remove(key);
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeBinary(const QString &key)
{
  target.binaryData.remove(key);
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeText(const QString &key)
{
  target.textData.remove(key);
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeLink(const QString &key)
{
  target.linksData.remove(key);
  return *this;
}

Memento::DataMutator &Memento::DataMutator::setData(const QString &key, const QString &data)
{
  target.textData[key] = data;
  return *this;
}

Memento::DataMutator &Memento::DataMutator::setRawLink(const QString &key, Memento *memObj)
{
  target.linksData[key] = memObj;
  return *this;
}

Memento::DataMutator &Memento::DataMutator::setLink(const QString &key, TreeNode *obj)
{
  if(auto el = createdMemento.find(obj); el != createdMemento.end())
    return setRawLink(key, el.value());
  else if(preserveExternalLinks && obj)
    target.externalLinksData[key] = obj;
  return *this;
}

Memento::DataMutator &Memento::DataMutator::createChildMemento(const QString &type)
{
  Memento *m;
  return createChildMemento(type, m);
}

Memento::DataMutator &Memento::DataMutator::createChildMemento(const QString &type, Memento *&out)
{
  target.children.push_back(out = new Memento(type));
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeChildAt(size_t i)
{
  target.children.remove(i);
  return *this;
}

Memento::DataMutator &Memento::DataMutator::removeAllChildren()
{
  target.children.clear();
  return *this;
}

const QHash<const TreeNode*, Memento*> &Memento::DataMutator::getCreatedMemento() const
{
  return createdMemento;
}
