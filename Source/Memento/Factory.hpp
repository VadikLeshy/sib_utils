#ifndef FACTORY_HPP
#define FACTORY_HPP

#include "../TreeNode.hpp"

namespace sib_utils::memento {
  class SIB_UTILS_SHARED_EXPORT Factory
  {
  public:
    Factory();
    ~Factory();

    void loadChildren(Memento &memento, TreeNode *node) const;
    TreeNode* loadObjectTree(Memento &memento, TreeNode *parent = nullptr) const;
    TreeNode* loadSingleObject(Memento &memento) const;

    Memento* saveObjectTreeBinary(const TreeNode &originator, bool preserveExternalLinks = false) const;
    Memento* saveObjectTreeText(const TreeNode &originator, bool preserveExternalLinks = false) const;

    template<typename T>
    void registerClass()
    {
      static_assert(std::is_base_of_v<TreeNode, T>, "T must be based on sib_utils::TreeNode");

      if constexpr(std::is_function_v<decltype(T::createFromMemento)>)
      {
        creators[T::staticMetaObject.className()] = &T::createFromMemento;
      } else {
        if constexpr(std::is_default_constructible<T>())
        {
          creators[T::staticMetaObject.className()] =
              [] (Memento::DataReader&) -> TreeNode* { return new T; };
        } else
        {
          static_assert (std::is_constructible<T, TreeNode*>() ||
                         std::is_constructible<T, QObject*>(), "Can not create T");

          creators[T::staticMetaObject.className()] =
              [] (Memento::DataReader&) -> TreeNode* { return new T(nullptr); };
        }
      }
    }



    //IMPLEMENT
  private:
    QHash<QString, std::function<TreeNode*(Memento::DataReader&)>> creators; //class name; creator

    TreeNode* restoreSingleObject(Memento::DataReader &memento, TreeNode *parentNode) const;

    inline TreeNode* restoreSingleObject(Memento::DataReader &&memento, TreeNode *parentNode) const
    { return restoreSingleObject(static_cast<Memento::DataReader&>(memento), parentNode); }

    Memento* saveObjectTree(const TreeNode &originator, void (TreeNode::*func)(Memento::DataMutator &&) const, bool preserveExternalLinks) const;
  };
}

#endif // FACTORY_HPP
