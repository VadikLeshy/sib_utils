#include "Factory.hpp"
#include <QtConcurrent>

using namespace sib_utils;
using namespace memento;

Factory::Factory()
{
}

Factory::~Factory()
{
}

void Factory::loadChildren(Memento &memento, TreeNode *node) const
{
  struct State {
    TreeNode *parent;
    Memento *memento;
  };
  QVector<State> stack;

  QHash<Memento*, TreeNode*> createdUnits = { {&memento, node} };

  {
    std::atomic_uint64_t i = stack.size();
    size_t n;
    auto reader = memento.reader(createdUnits);
    reader.getChildrenCount(n);
    stack.resize(i + n);
    reader.forallChildren([&stack, node, &i] (Memento &childMemento) {
        stack[i++] = State {node, &childMemento};
      });
  }

  while(!stack.isEmpty())
    {
      auto m = stack.takeLast();
      auto reader = m.memento->reader(createdUnits);

      if(auto obj = restoreSingleObject(reader, m.parent); obj)
        {
          createdUnits.insert(m.memento, obj);

          std::atomic_uint64_t i = stack.size();
          size_t n;
          reader.getChildrenCount(n);
          stack.resize(i + n);
          reader.forallChildren([&stack, obj, &i] (Memento &childMemento) {
              stack[i++] = State {obj, &childMemento};
            });
        }
    }

  for(auto i = createdUnits.begin(); i != createdUnits.end(); )
    {
      try
      {
        (*i)->load(i.key()->reader(createdUnits));
        ++i;
      } catch(...)
      {
        delete *i;
        i = createdUnits.erase(i);
      }
    }
}

TreeNode *Factory::loadObjectTree(Memento &memento, TreeNode *parent) const
{
  auto reader = memento.reader();

  TreeNode *result = restoreSingleObject(reader, parent);
  loadChildren(memento, result);
  return result;
}

TreeNode *Factory::loadSingleObject(Memento &memento) const
{
  auto reader = memento.reader();
  if(auto obj = restoreSingleObject(reader, nullptr); obj)
    {
      try
      {
        obj->load(reader);
        return obj;
      }
      catch(...)
      {
        delete obj;
        return nullptr;
      }
    } else
    return nullptr;
}

Memento *Factory::saveObjectTreeBinary(const TreeNode &originator, bool preserveExternalLinks) const
{
  return saveObjectTree(originator, static_cast<void (TreeNode::*)(Memento::DataMutator &&) const>(&TreeNode::saveBinary), preserveExternalLinks);
}

Memento *Factory::saveObjectTreeText(const TreeNode &originator, bool preserveExternalLinks) const
{
  return saveObjectTree(originator, static_cast<void (TreeNode::*)(Memento::DataMutator &&) const>(&TreeNode::saveText), preserveExternalLinks);
}

TreeNode *Factory::restoreSingleObject(Memento::DataReader &memento, TreeNode *parentNode) const
{
  TreeNode *result = nullptr;

  memento.readType([this, &memento, parentNode, &result] (const QString &type) {
      if(auto el = creators.find(type); el != creators.end())
        {
          auto dataUnit = (*el)(memento);
          if(dataUnit && parentNode)
            dataUnit->updateParent(parentNode);
          result = dataUnit;
        }
    });

  return result;
}

Memento *Factory::saveObjectTree(const TreeNode &originator, void (TreeNode::*func)(Memento::DataMutator &&) const, bool preserveExternalLinks) const
{
  Memento *result = nullptr;

  QHash<const TreeNode*, Memento*> createdMemento;

  struct State {
    Memento *parentMemento;
    const TreeNode *dataUnit;
  };

  std::list<State> stack { State{nullptr, &originator} };

  while(!stack.empty())
    {
      auto el = stack.back();
      stack.pop_back();

      Memento *memento;
      if(el.parentMemento)
        el.parentMemento->mutator(createdMemento, preserveExternalLinks).createChildMemento(el.dataUnit->getMementoType(), memento);
      else
        memento = new Memento(el.dataUnit->getMementoType());

      if(!result)
        result = memento;

      createdMemento.insert(el.dataUnit, memento);

      QSet<QObject*> excludedChildren = ([](const auto &a) {
          QSet<QObject*> result;
          for(const QVariant &i: a)
            result.insert(i.value<QObject*>());
          return result;
        })(el.dataUnit->property("ExcludedChildren").value<QList<QVariant>>());

      for(auto *child: el.dataUnit->children())
        if(auto childData = dynamic_cast<TreeNode*>(child); childData && !excludedChildren.contains(child))
          stack.push_back(State {memento, childData});
    }

  std::vector<QFuture<void>> tasks;

  for(auto i = createdMemento.begin(); i != createdMemento.end(); ++i)
    tasks.push_back(QtConcurrent::run([i, &createdMemento, func, preserveExternalLinks] { (i.key()->*func)(i.value()->mutator(createdMemento, preserveExternalLinks)); }));

  for(auto &i: tasks)
    i.waitForFinished();

  return result;
}
