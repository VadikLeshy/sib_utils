#ifndef BLOBSAVER_HPP
#define BLOBSAVER_HPP

#include "Memento.hpp"
#include <QIODevice>

namespace sib_utils::memento {
  class SIB_UTILS_SHARED_EXPORT BlobSaver
  {
  public:
    BlobSaver();

    void save(QIODevice &dev, const Memento &mem);

    inline void save(QIODevice &&dev, const Memento &mem)
    { return save(static_cast<QIODevice&>(dev), mem); }

    void save(QByteArray &out, const Memento &mem);
  };
}

#endif // BLOBSAVER_HPP
