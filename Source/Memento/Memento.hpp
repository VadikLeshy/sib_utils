#ifndef MEMENTO_HPP
#define MEMENTO_HPP

#include <QHash>
#include <QString>
#include <QVector>
#include <QObject>
#include <QByteArray>
#include <functional>
#include <QReadWriteLock>
#include "../sib_utils_global.hpp"

namespace sib_utils {
  class TreeNode;

  namespace memento {
    class SIB_UTILS_SHARED_EXPORT Memento
    {
      friend class MementoFactory;

      Memento(const Memento &) = delete;
      Memento& operator=(const Memento &) = delete;

    public:
      explicit Memento(const QString &type = "Object");
      ~Memento();

      class SIB_UTILS_SHARED_EXPORT DataMutator {
        QWriteLocker l;
        Memento &target;
        const QHash<const TreeNode*, Memento*> &createdMemento;
      public:
        const bool preserveExternalLinks;

        DataMutator(Memento &target, const QHash<const TreeNode*, Memento*> &createdMemento, bool preserveExternalLinks);
        ~DataMutator() = default;

        DataMutator& setData(const QString &key, const QByteArray &data);
        DataMutator& setData(const QString &key, const QString &data);

        DataMutator& setRawLink(const QString &key, Memento *memObj);
        DataMutator& setLink(const QString &key, TreeNode *obj);

        DataMutator& removeData(const QString &key);
        DataMutator& removeBinary(const QString &key);
        DataMutator& removeText(const QString &key);
        DataMutator& removeLink(const QString &key);

        DataMutator& createChildMemento(const QString &type);
        DataMutator& createChildMemento(const QString &type, Memento *&out);
        DataMutator& removeChildAt(size_t i);
        DataMutator& removeAllChildren();

        const QHash<const TreeNode*, Memento*> &getCreatedMemento() const;
      };

      class SIB_UTILS_SHARED_EXPORT DataReader {
        QReadLocker l;
        Memento &target;
        const QHash<Memento*, TreeNode*> &createdObjects;

      public:
        DataReader(Memento &target, const QHash<Memento*, TreeNode*> &createdObjects);
        ~DataReader() = default;

        DataReader& get(const QString &key, QByteArray &out, bool &readed);
        DataReader& get(const QString &key, QString &out, bool &readed);
        DataReader& get(const QString &key, TreeNode *&out, bool &readed);
        DataReader& getRawLink(const QString &key, Memento *&out, bool &readed);

        DataReader& read(const QString &key, std::function<void (const QByteArray&)> &&func);
        DataReader& readText(const QString &key, std::function<void (const QString&)> &&func);
        DataReader& read(const QString &key, std::function<void (TreeNode*)> &&func);

        DataReader& readAll(std::function<void (const QString &, const QByteArray&)> &&func);
        DataReader& readAllText(std::function<void (const QString &, const QString&)> &&func);
        DataReader& readAllRawLinks(std::function<void (const QString &, Memento*)> &&func);
        DataReader& readAll(std::function<void (const QString &, TreeNode*)> &&func);

        DataReader& forallChildren(std::function<void(Memento&)> &&func);
        DataReader& forallChildrenR(std::function<void(Memento&)> &&func);
        DataReader& getChildrenCount(size_t &count);
        DataReader& getChild(size_t i, Memento *&result);

        DataReader& getType(QString &out);
        DataReader& readType(std::function<void(const QString&)> &&func);

        const QHash<Memento*, TreeNode*> &getCreatedObjects() const;
      };

      DataReader reader(const QHash<Memento*, TreeNode*> &createdObjects);
      DataMutator mutator(const QHash<const TreeNode*, Memento*> &createdMemento, bool preserveExternalLinks = false);

      DataReader reader();
      DataMutator mutator(bool preserveExternalLinks = false);



      //IMPLEMENT
    private:
      QReadWriteLock lock;

      QHash<QString, QByteArray> binaryData;
      QHash<QString, QString> textData;
      QHash<QString, Memento*> linksData;
      QHash<QString, TreeNode*> externalLinksData;

      QVector<Memento*> children;

      QString type;
    };
  }
}

#endif // MEMENTO_HPP
