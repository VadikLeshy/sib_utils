#ifndef MATH_HPP
#define MATH_HPP

#include <type_traits>
#include <limits>
#include <math.h>

namespace sib_utils {
  class Math
  {
    Math();
  public:
    template<typename T, class = std::enable_if_t<!std::numeric_limits<T>::is_integer>>
    static bool almost_equal(T x, T y, int ulp)
    {
      using namespace std;
      // epsilon should be scaled to max and multiplied by ULP (units in the last place)
      return abs(x-y) <= numeric_limits<T>::epsilon()
          * max(abs(x), abs(y))
          * ulp;
    }
  };
}

#endif // MATH_HPP
