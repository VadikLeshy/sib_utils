#ifndef TREENODE_HPP
#define TREENODE_HPP

#include <memory>
#include <QObject>
#include "Memento/Memento.hpp"
#include "call_tuple_arg.hpp"
#include "refine_type.hpp"

namespace sib_utils {
  namespace memento { class Factory; }

  class SIB_UTILS_SHARED_EXPORT TreeNode: public QObject
  {
    Q_OBJECT
    friend class memento::Factory;

    struct PrivateSignalArg {};
    PrivateSignalArg createFromMemento;

    static constexpr char nodeType[] = "Object";
    Q_PROPERTY(const QString MementoType MEMBER nodeType DESIGNABLE false CONSTANT);

  public:
    TreeNode();
    virtual ~TreeNode() = default;

    void saveBinary(memento::Memento::DataMutator &memento) const;
    void saveText(memento::Memento::DataMutator &memento) const;
    void load(memento::Memento::DataReader &memento);

    inline void saveBinary(memento::Memento::DataMutator &&memento) const
    { saveBinary(static_cast<memento::Memento::DataMutator&>(memento)); }

    inline void saveText(memento::Memento::DataMutator &&memento) const
    { saveText(static_cast<memento::Memento::DataMutator&>(memento)); }

    inline void load(memento::Memento::DataReader &&memento)
    { load(static_cast<memento::Memento::DataReader&>(memento)); }

    void updateParent(TreeNode *parent);

    template<typename T, class = std::enable_if_t<std::is_base_of_v<sib_utils::TreeNode, T>>>
    void updateParent(const std::unique_ptr<T> &parent)
    {
      updateParent(parent.get());
    }

    template<typename T, class = std::enable_if_t<std::is_base_of_v<sib_utils::TreeNode, T>>>
    void updateParent(const std::shared_ptr<T> &parent)
    {
      updateParent(parent.get());
    }

    template<typename T, class = std::enable_if_t<std::is_base_of_v<sib_utils::TreeNode, T>>>
    void updateParent(const std::weak_ptr<T> &parent)
    {
      updateParent(parent.get());
    }

    template <typename T, typename... ArgsT, class = std::enable_if_t<std::is_constructible_v<refined_type<T>, ArgsT...>>>
    refined_type<T>* createChild(ArgsT&&... args)
    {
      using U = refined_type<T>;
      static_assert(std::is_base_of<TreeNode, U>::value, "T must be TreeNode based class");

      auto child = new U(std::forward<ArgsT>(args)...);
      child->updateParent(this);
      return child;
    }

    template <typename T, typename... ArgsT, class = std::enable_if_t<std::is_constructible_v<refined_type<T>, ArgsT...>>>
    std::unique_ptr<refined_type<T>> createChild_uptr(ArgsT&&... args)
    {
      using U = refined_type<T>;
      static_assert(std::is_base_of<TreeNode, U>::value, "T must be TreeNode based class");

      auto child = std::make_unique<U>(std::forward<ArgsT>(args)...);
      child->updateParent(this);
      return child;
    }

    template <typename T, typename... ArgsT, class = std::enable_if_t<std::is_constructible_v<refined_type<T>, ArgsT...>>>
    std::shared_ptr<refined_type<T>> createChild_sptr(ArgsT&&... args)
    {
      using U = refined_type<T>;
      static_assert(std::is_base_of<TreeNode, U>::value, "T must be TreeNode based class");

      auto child = std::make_shared<U>(std::forward<ArgsT>(args)...);
      child->updateParent(this);
      return child;
    }

    template <typename T, typename... ArgsT, class = std::enable_if_t<std::is_constructible_v<refined_type<T>, ArgsT...>>>
    refined_type<T>* findOrCreateChild(std::tuple<ArgsT...> &&args, const QString &name = QString())
    {
      using U = refined_type<T>;

      auto child = findChild<U*>(name, Qt::FindDirectChildrenOnly);
      if(!child)
        {
          child = call_tuple_arg(&TreeNode::createChild<U, ArgsT...>, std::move(args), this);
          if(!name.isEmpty() && !name.isNull())
            child->setObjectName(name);
        }
      return child;
    }

    template <typename T, class = std::enable_if_t<std::is_default_constructible_v<refined_type<T>>>>
    refined_type<T>* findOrCreateChild(const QString &name = QString())
    {
      using U = refined_type<T>;

      auto child = findChild<U*>(name, Qt::FindDirectChildrenOnly);
      if(!child)
        {
          child = createChild<U>();
          if(!name.isEmpty() && !name.isNull())
            child->setObjectName(name);
        }
      return child;
    }

    QString getMementoType() const;

  protected:
    inline virtual void writeBinary(memento::Memento::DataMutator&) const {}
    inline virtual void writeText(memento::Memento::DataMutator&) const {}
    inline virtual void read(memento::Memento::DataReader&) {}

    // QObject interface
  protected:
    void childEvent(QChildEvent *event) override;

  signals:
    void parentUpdated(PrivateSignalArg *arg);

    void childAdded(TreeNode *obj);
    void childRemoved(TreeNode *obj);



    // IMPLEMENT
  private:
    void updateParent();
  };
}

#endif // TREENODE_HPP
