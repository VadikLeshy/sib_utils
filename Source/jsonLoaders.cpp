#include "jsonLoaders.hpp"
#include <QJsonObject>
#include <QJsonArray>

glm::vec2 sib_utils::json::readVec2(const QJsonValue &val, const glm::vec2 &defaultVal)
{
  if(val.isArray())
    {
      const auto elems = val.toArray();
      if(elems.size() == 1 && elems[0].isDouble())
        return glm::vec2(elems[0].toDouble());
      else if(elems.size() == 2)
        return glm::vec2(elems[0].toDouble(defaultVal.x), elems[1].toDouble(defaultVal.y));
      else
        return defaultVal;
    } else if(val.isObject())
    {
      const auto obj = val.toObject();
      return glm::vec2(obj.value(QLatin1String("x")).toDouble(defaultVal.x), obj.value(QLatin1String("y")).toDouble(defaultVal.y));
    } else
    return defaultVal;
}

glm::vec3 sib_utils::json::readVec3(const QJsonValue &val, const glm::vec3 &defaultVal)
{
  if(val.isArray())
    {
      const auto elems = val.toArray();
      if(elems.size() == 1 && elems[0].isDouble())
        return glm::vec3(elems[0].toDouble());
      else if(elems.size() == 3)
        return glm::vec3(elems[0].toDouble(defaultVal.x), elems[1].toDouble(defaultVal.y), elems[2].toDouble(defaultVal.z));
      else
        return defaultVal;
    } else if(val.isObject())
    {
      const auto obj = val.toObject();
      return glm::vec3(obj.value(QLatin1String("x")).toDouble(defaultVal.x), obj.value(QLatin1String("y")).toDouble(defaultVal.y), obj.value(QLatin1String("z")).toDouble(defaultVal.z));
    } else
    return defaultVal;
}

glm::vec4 sib_utils::json::readVec4(const QJsonValue &val, const glm::vec4 &defaultVal)
{
  if(val.isArray())
    {
      const auto elems = val.toArray();
      if(elems.size() == 1 && elems[0].isDouble())
        return glm::vec4(elems[0].toDouble());
      else if(elems.size() == 4)
        return glm::vec4(elems[0].toDouble(defaultVal.x), elems[1].toDouble(defaultVal.y), elems[2].toDouble(defaultVal.z), elems[3].toDouble(defaultVal.w));
      else
        return defaultVal;
    } else if(val.isObject())
    {
      const auto obj = val.toObject();
      return glm::vec4(obj.value(QLatin1String("x")).toDouble(defaultVal.x), obj.value(QLatin1String("y")).toDouble(defaultVal.y), obj.value(QLatin1String("z")).toDouble(defaultVal.z), obj.value(QLatin1String("w")).toDouble(defaultVal.w));
    } else
    return defaultVal;
}

glm::mat4 sib_utils::json::readMat4(const QJsonValue &val, const glm::mat4 &defaultVal)
{
  if(!val.isArray())
    return defaultVal;

  const auto elems = val.toArray();
  if(elems.size() == 1 && elems[0].isDouble())
    return glm::mat4(elems[0].toDouble());
  else if(elems.size() == 16)
    {
      return glm::mat4
          (elems[0].toDouble(defaultVal[0][0]), elems[1].toDouble(defaultVal[0][1]), elems[2].toDouble(defaultVal[0][2]), elems[3].toDouble(defaultVal[0][3]),
          elems[4].toDouble(defaultVal[1][0]), elems[5].toDouble(defaultVal[1][1]), elems[6].toDouble(defaultVal[1][2]), elems[7].toDouble(defaultVal[1][3]),
          elems[8].toDouble(defaultVal[2][0]), elems[9].toDouble(defaultVal[2][1]), elems[10].toDouble(defaultVal[2][2]), elems[11].toDouble(defaultVal[2][3]),
          elems[12].toDouble(defaultVal[3][0]), elems[13].toDouble(defaultVal[3][1]), elems[14].toDouble(defaultVal[3][2]), elems[15].toDouble(defaultVal[3][3]));
    } else
    return defaultVal;
}
