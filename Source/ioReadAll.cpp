#include "ioReadAll.hpp"
#include "IoScopedState.hpp"

QByteArray sib_utils::ioReadAll(QIODevice &io, QIODevice::OpenMode state)
{
  IoScopedState st(io, state);
  return io.readAll();
}
