#ifndef REFINETYPE_HPP
#define REFINETYPE_HPP

#include <type_traits>

namespace sib_utils
{
  template<typename U>
  struct refine_type
  {
    using type = std::remove_cv_t<U>;
  };

  template<typename U>
  struct refine_type<U*>
  {
    using type = typename refine_type<std::remove_pointer_t<U>>::type;
  };

  template<typename U>
  struct refine_type<U&>
  {
    using type = typename refine_type<std::remove_reference_t<U>>::type;
  };

  template<typename U>
  struct refine_type<U&&>
  {
    using type = typename refine_type<std::remove_reference_t<U>>::type;
  };

  template<typename U>
  struct refine_type<const U&>
  {
    using type = typename refine_type<std::remove_reference_t<U>>::type;
  };

  template<typename T>
  using refined_type = typename refine_type<T>::type;

  static_assert(std::is_same_v<refined_type<int>, int>);
  static_assert(std::is_same_v<refined_type<int*>, int>);
  static_assert(std::is_same_v<refined_type<const int*>, int>);
  static_assert(std::is_same_v<refined_type<volatile int*>, int>);
  static_assert(std::is_same_v<refined_type<const volatile int*>, int>);
  static_assert(std::is_same_v<refined_type<int* const *>, int>);
  static_assert(std::is_same_v<refined_type<int**const***>, int>);

  static_assert(std::is_same_v<refined_type<int&>, int>);
  static_assert(std::is_same_v<refined_type<const int&>, int>);
  static_assert(std::is_same_v<refined_type<volatile int&>, int>);
  static_assert(std::is_same_v<refined_type<const volatile int&>, int>);
  static_assert(std::is_same_v<refined_type<int* const &>, int>);
  static_assert(std::is_same_v<refined_type<int&&>, int>);
}


#endif // REFINETYPE_HPP
