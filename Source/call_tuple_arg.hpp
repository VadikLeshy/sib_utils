#ifndef CALL_TUPLE_ARG_HPP
#define CALL_TUPLE_ARG_HPP

#include <type_traits>
#include <functional>
#include <tuple>
#include "refine_type.hpp"

namespace sib_utils
{
  class TupleCallerPrivate {
    template<typename Func, typename Tuple, typename... Head>
    friend constexpr auto call_tuple_arg(Func &&f, Tuple &&tuple, Head&&... head);

    template<typename Func, typename Tuple, typename... Head, size_t... N>
    static constexpr auto call(Func &&f, Tuple &&tuple, Head&&... head, const std::index_sequence<N...> &)
    {
      return std::invoke(f, std::forward<Head>(head)..., std::get<N>(std::forward<Tuple>(tuple))...);
    }
  };

  template<typename Func, typename Tuple, typename... Head>
  constexpr auto call_tuple_arg(Func &&f, Tuple &&tuple, Head&&... head)
  {
    return TupleCallerPrivate::call<Func, Tuple, Head...>
        (std::forward<Func>(f), std::forward<Tuple>(tuple), std::forward<Head>(head)..., std::make_index_sequence<std::tuple_size_v<refined_type<Tuple>>>());
  }
}

#endif // CALL_TUPLE_ARG_HPP
