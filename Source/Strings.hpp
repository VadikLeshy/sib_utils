#ifndef STRINGS_HPP
#define STRINGS_HPP

#include "sib_utils_global.hpp"
#include <QString>
#include <array>

namespace sib_utils::strings {
  template<size_t N0, size_t N1, size_t... Ind0, size_t... Ind1>
  inline constexpr auto concat(const char (&str0)[N0], const char (&str1)[N1], std::index_sequence<Ind0...>, std::index_sequence<Ind1...>)
  -> std::array<char, N0+N1-1>
  {
    return {{str0[Ind0]..., str1[Ind1]...}};
  }

  template<size_t N0, size_t N1>
  inline constexpr auto concat(const char (&str0)[N0], const char (&str1)[N1])
  -> std::array<char, N0+N1-1>
  {
    return concat(str0, str1, std::make_index_sequence<N0-1>(), std::make_index_sequence<N1>());
  }

  template<size_t N>
  inline QLatin1String getQLatin1Str(const std::array<char, N> &arr)
  {
    if(Q_LIKELY(arr.back() != '\0'))
      return QLatin1String(arr.data(), N);
    else
      return QLatin1String(arr.data(), N-1);
  }

  template<size_t N>
  inline QLatin1String getQLatin1Str(const char (&arr)[N])
  {
    if(Q_LIKELY(arr[N-1] == '\0'))
      return QLatin1String(&arr[0], N-1);
    else
      return QLatin1String(&arr[0], N);
  }

  template<size_t N>
  inline QString getQStr(const std::array<char, N> &arr)
  {
    if(Q_LIKELY(arr.back() != '\0'))
      return QString(QByteArray::fromRawData(arr.data(), N));
    else
      return QString(QByteArray::fromRawData(arr.data(), N-1));
  }

  template<size_t N>
  inline QString getQStr(const char (&arr)[N])
  {
    if(Q_LIKELY(arr[N-1] == '\0'))
      return QString(QByteArray::fromRawData(&arr[0], N-1));
    else
      return QString(QByteArray::fromRawData(&arr[0], N));
  }

  SIB_UTILS_SHARED_EXPORT QString getQStr(const std::string_view &str);
}

#endif // STRINGS_HPP
